//
//  ViewController.m
//  ParseSaveDataDemo
//
//  Created by joser on 13-8-19.
//  Copyright (c) 2013年 joser. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>
@interface ViewController ()

@end

@implementation ViewController
static int a=0;
- (void)viewDidLoad
{
    [super viewDidLoad];
//    //1 可以通过下面的方法保存数据
//    PFObject *gameScore = [PFObject objectWithClassName:@"GameScore"];
//    [gameScore setObject:[NSNumber numberWithInt:1337] forKey:@"score"];
//    [gameScore setObject:@"Sean Plott" forKey:@"playerName"];
//    [gameScore setObject:[NSNumber numberWithBool:NO] forKey:@"cheatMode"];
//
//    //one save fountion
//    [gameScore saveInBackground];
//    //保存的数据类型只能是 strings, numbers, booleans,  arrays , dictionaries - 任何可以被json解析的。
//    //也可以使用下面的两个方法在保存之后执行其他的方法
//    // two save fountion
//    [gameScore saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        a++;
//        NSLog(@"%d",a);
//
//        if(a==3)
//        {
//            DEFAULT_ALERTVIEW(@"save ok")
//            
//        }
//        
//
//    }];
//    // three save fountion
//    [gameScore saveInBackgroundWithTarget:self selector:@selector(SaveOk)];
// //2 可以通过下面的方法获取对应ID的数据
//    PFQuery *query = [PFQuery queryWithClassName:@"GameScore"];
//    // this one
//    __block PFObject *resoultObject=nil;
//    [query getObjectInBackgroundWithId:@"HJVAPpt6ae" block:^(PFObject *gameScore, NSError *error) {
//        // Do something with the returned PFObject in the gameScore variable.
//        NSLog(@"%@", gameScore);
//        resoultObject=gameScore;
//    }];
//    
//    // this two
//    [query getObjectInBackgroundWithId:@"HJVAPpt6ae" target:self selector:@selector(gameScore:error:)];
//    // 3 下面是获取对应数据库的值
//
//    int score = [[resoultObject objectForKey:@"score"] intValue];
//    NSString *playerName = [resoultObject objectForKey:@"playerName"];
//    BOOL cheatMode = [[resoultObject objectForKey:@"cheatMode"] boolValue];
//    //下面得到的数据是自动生成的。
//
//    NSString *objectId = gameScore.objectId;
//    NSDate *updatedAt = gameScore.updatedAt;
//    NSDate *createdAt = gameScore.createdAt;
//
//    //下面是对于block的说明；
//    //这个 block方法可以替代delegate方法
//    [self addBlock:^void(int a, int b) {
//        NSLog(@"%d",a+b);
//    }];
//   // 这个是没有返回值
//    void (^block2)(int a,int b)=^(int a,int b)
//    {
//       
//        NSLog(@"%d",a+b);
//
//    };
//    block2(2,3);
//    //这个是带有返回值的
//    int (^block3)(int a,int b)=^(int a,int b)
//    {
//        return a+b;
//
//    };
//    int c=block3(3,4);
//     NSLog(@"%d",c);
//    //下面是怎么修改外部的变量
//    __block int d=4;
//    int (^block4)(int a,int b)=^(int a,int b)
//    {
//        return a+b;
//        d++;
//
//    };
//    NSLog(@"%d,%d",block4(2,6),d);
//   
//    //下面是怎么刷新数据
//    
//    [resoultObject refresh];
//    //如果我们有时候突然离线，该怎么保存数据，使用下面的方法
//    void (^saveEventually)(void)=^(void)
//    {
//        PFObject *gameScore = [PFObject objectWithClassName:@"GameScore"];
//        [gameScore setObject:[NSNumber numberWithInt:1337] forKey:@"score"];
//        [gameScore setObject:@"Sean Plott" forKey:@"playerName"];
//        [gameScore setObject:[NSNumber numberWithBool:NO] forKey:@"cheatMode"];
//        [gameScore saveEventually];
//
//    };
//    saveEventually();
//    //下面是怎么修改数据库里面的数据
//    void (^ editBlock)(void)=^(void)
//    {
//        PFQuery *query = [PFQuery queryWithClassName:@"GameScore"];
//
//        // Retrieve the object by id
//        [query getObjectInBackgroundWithId:@"xWMyZ4YEGZ" block:^(PFObject *gameScore, NSError *error) {
//
//            // Now let's update it with some new data. In this case, only cheatMode and score
//            // will get sent to the cloud. playerName hasn't changed.
//            [gameScore setObject:[NSNumber numberWithBool:YES] forKey:@"cheatMode"];
//            [gameScore setObject:[NSNumber numberWithInt:1338] forKey:@"score"];
//            [gameScore saveInBackground];
//            
//        }];
//
//    };
//    editBlock();
//    //下面是只修改一个字段的数据
//    void (^editAData)(int score)=^(int score)
//    {
//
//        [gameScore incrementKey:@"score"];
//        [gameScore saveInBackground];
//
//    };
//    editAData(2000);
    void (^ block5)(void)=^(void)
    {
        [self gameScore:nil error:nil];
    };
    block5();
}
-(void)gameScore:(PFObject *)gameScore error:(NSError *)error
{
   NSLog(@"%@", @"1233");
    
      
}

-(void)addBlock:(newBlock)block
{
   
    block(1,2);

}

-(void)SaveOk
{
      a++;
      NSLog(@"%d",a);
   
      if(a==3)
      {
         DEFAULT_ALERTVIEW(@"save ok")

      }
    
     
    


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
